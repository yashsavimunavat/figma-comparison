// set up some sample squares with random colors
var canvasObj1 = document.getElementById('canvas1');
var context1 = canvasObj1.getContext('2d');
var img1 = document.getElementById('img1').value;
var img2 = document.getElementById('img2').value;
img1.crossOrigin = "Anonymous";
img2.crossOrigin = "Anonymous";

//alert(img1);

base_image1 = new Image();
  base_image1.src = img1;
  base_image1.crossOrigin = "Anonymous";
  base_image1.onload = function(){
//    alert('onload');
    context1.drawImage(base_image1, 0, 0);
  }


var canvasObj2 = document.getElementById('canvas2');
var context2 = canvasObj2.getContext('2d');
    base_image2 = new Image();
  base_image2.src = img2;
  base_image2.crossOrigin = "Anonymous";
  base_image2.onload = function(){
//    alert('onload');
    context2.drawImage(base_image2, 0, 0);
  }

  // context.fillStyle = randomColor();
// context.fillRect(0, 0, 50, 50);
// context.fillStyle = randomColor();
// context.fillRect(55, 0, 50, 50);
// context.fillStyle = randomColor();
// context.fillRect(110, 0, 50, 50);

$('#canvas1').mousemove(function(e) {
    var pos = findPos(this);
    var x = e.pageX - pos.x;
    var y = e.pageY - pos.y;
    var coord = "x=" + x + ", y=" + y;
    var c = this.getContext('2d');
    var p = c.getImageData(x, y, 1, 1).data;
    var hex = "#" + ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6);
    $('#status1').html(coord + "  |  " + hex);
    $('#status1').css('background',hex);

    // var c = this.getContext('2d');
    p = context2.getImageData(x, y, 1, 1).data;
    hex = "#" + ("000000" + rgbToHex(p[0], p[1], p[2])).slice(-6);
    $('#status2').html(coord + "  |  " + hex);
    $('#status2').css('background',hex);
});

function findPos(obj) {
    var curleft = 0, curtop = 0;
    if (obj.offsetParent) {
        do {
            curleft += obj.offsetLeft;
            curtop += obj.offsetTop;
        } while (obj = obj.offsetParent);
        return { x: curleft, y: curtop };
    }
    return undefined;
}

function rgbToHex(r, g, b) {
    if (r > 255 || g > 255 || b > 255)
        throw "Invalid color component";
    return ((r << 16) | (g << 8) | b).toString(16);
}

function randomInt(max) {
  return Math.floor(Math.random() * max);
}

function randomColor() {
	return `rgb(${randomInt(256)}, ${randomInt(256)}, ${randomInt(256)})`
}